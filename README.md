# Ajira Food Backend
This is a Spring Boot Application used to manage and monitor the food consumptions of all users in Ajira. It is a simple application which consists of small endpoints to perform the specific tasks.

## Prerequisites
To run the application, you need to install the following:
- Git.
- JDK.
- Postgresql.
- Any IDE of your choice.
- Postman (Optional).

## Getting Started
To get started clone the repository into local machine by using the following command:
```
git clone https://gitlab.com/ArunPandianApk/FoodBackEnd.git
```
This will create a clone of the repository in the folder FoodBackEnd.

## Running the Application
The application can be opened and run through the IDE or by building and running Maven.

### Hitting Endpoints
The endpoints of the application have specific functions associated with them. They can be exposed by hitting the URL. This can be done through 2 ways.

#### Through Postman/ Browser:
- Open Postman or Browser
- Enter the URL of the endpoint. (Eg: 'http://localhost:8080/users')
- The Response can be seen in the Response window in Postman or the page body in the browser.

**Note:** Postman is prefered because Most Request methods other than GET require some extensions in the browsers.

#### Through Curl:
- Open Terminal
- Type the curl command as given below:
```
curl http://localhost:8080/users
```
- The Response can be seen in the Terminal window.

The above URL endpoint gets a JSON of the details of all users.
