package com.project.test.servicetests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import food.com.project.model.Consumption;
import food.com.project.repository.ConsumptionRepository;
import food.com.project.services.ConsumptionServices;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsumptionServicesTest 
{
	@InjectMocks
	ConsumptionServices services;
	
	@Mock
	ConsumptionRepository repository;
	
	@Test
	public void testAddConsumption()
	{
		Consumption consumption = mock (Consumption.class);
		services.addConsumption(consumption);
		verify(repository).save(consumption);
	}
}