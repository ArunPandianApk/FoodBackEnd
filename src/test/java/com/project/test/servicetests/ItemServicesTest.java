package com.project.test.servicetests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import food.com.project.model.Item;
import food.com.project.repository.ItemRepository;
import food.com.project.services.ItemServices;

@RunWith(SpringJUnit4ClassRunner.class)
public class ItemServicesTest 
{
	@InjectMocks
	ItemServices services;
	
	@Mock
	ItemRepository repository;
	
	@Test
	public void testGetItems()
	{
		services.getAllItems();
		verify(repository).findAllOrderByName();
	}
	
	@Test
	public void testGetActiveItems()
	{
		services.getItems();
		verify(repository).findActive();
	}
	
	@Test
	public void testDeactivateItems()
	{
		services.deactivateItem(0);
		verify(repository).stopDelivery(0);
	}
	
	@Test
	public void testActivateItems()
	{
		services.activateItem(0);
		verify(repository).startDelivery(0);
	}
	
	@Test
	public void testAddItems()
	{
		Item item = mock(Item.class);
		services.addItem(item);
		verify(repository).save(item);
	}
	
	@Test
	public void testDeleteItems()
	{
		services.deleteItem(0);
		verify(repository).deleteById((long) 0);
	}
	
	@Test
	public void testGetItem()
	{
		String name = "Arun Pandian";
		services.getIdByName(name);
		verify(repository).getIdByName(name);
	}
	
	@Test
	public void testItemNames()
	{
		services.getItemsName();
		verify(repository).getNames();
	}
}