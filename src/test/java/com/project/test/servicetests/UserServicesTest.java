package com.project.test.servicetests;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import food.com.project.model.Users;
import food.com.project.repository.UserRepository;
import food.com.project.services.UserServices;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServicesTest 
{
	@InjectMocks
	UserServices services;
	
	@Mock
	UserRepository repository;
	
	@Test
	public void testGetUsers()
	{
		services.getUsers();
		verify(repository).findActive();
	}
	
	@Test
	public void testGetOrdered()
	{
		services.getUsersOrderByName();
		verify(repository).findAllByName();
	}
	
	@Test
	public void testGetUsernames()
	{
		services.getUserNames();
		verify(repository).findActiveNames();
	}
	
	@Test
	public void testAddUsers()
	{
		Users users = mock(Users.class);
		services.addUser(users);
		verify(repository).save(users);
	}
	
	@Test
	public void testDeactivateUsers()
	{
		services.deactivateUser(0);
		verify(repository).deactivate(0);
	}
	
	@Test
	public void testActivateUsers()
	{
		services.activateUser(0);
		verify(repository).activate(0);
	}
}