package com.project.test.servicetests;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import food.com.project.repository.SummaryReportRepository;
import food.com.project.services.SummaryReportServices;

@RunWith(SpringJUnit4ClassRunner.class)
public class SummaryReportServicesTest 
{
	@InjectMocks
	SummaryReportServices services;
	
	@Mock
	SummaryReportRepository repository;
	
	@Test
	public void testGetSummary()
	{
		services.getSummary();
		verify(repository).getSummary();
	}
	
	@Test
	public void testGetOverallSummary()
	{
		services.getOverallSummary();
		verify(repository).getOverallSummary();
	}
}