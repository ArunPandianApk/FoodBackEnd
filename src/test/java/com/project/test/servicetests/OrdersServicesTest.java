package com.project.test.servicetests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import food.com.project.model.Orders;
import food.com.project.repository.OrdersRepository;
import food.com.project.services.OrdersServices;

@RunWith(SpringJUnit4ClassRunner.class)
public class OrdersServicesTest 
{
	@InjectMocks
	OrdersServices services;
	
	@Mock
	OrdersRepository repository;
	
	@Test
	public void testGetUsers()
	{
		services.getLog();
		verify(repository).findAll();
	}
	
	@Test
	public void testAddOrder()
	{
		Orders order = mock(Orders.class);
		services.addOrders(order);
		verify(repository).save(order);
	}
	
	@Test
	public void testEndOrder()
	{
		services.endSession();
		verify(repository).endSession();
	}
}
	