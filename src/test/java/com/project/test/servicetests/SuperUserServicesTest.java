package com.project.test.servicetests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import food.com.project.model.SuperUser;
import food.com.project.repository.SuperUserRepository;
import food.com.project.services.SuperUserServices;

import java.util.Arrays;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
public class SuperUserServicesTest 
{
	@InjectMocks
	SuperUserServices services;
	
	@Mock
	SuperUserRepository repository;

	@Test
	public void testGetUsers()
	{
		SuperUser superUser = services.getSuperUser(1);
		SuperUser superUser1 = (repository).getOne((long) 1);
		if(superUser1 == null)
		    superUser1 = new SuperUser();
		Assert.assertEquals(superUser.getName() ,superUser1.getName());
	}
	
	@Test
	public void testAddUsers()
	{
		SuperUser superUser = mock(SuperUser.class);
		services.addUser(superUser);
		verify(repository).save(superUser);
	}

	@Test
	public void testLogout()
	{
		services.logout();
		verify(repository).logout();
	}
}