package food.com.project.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Consumption 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	public long id;
	public long userId;
	public Date date;
	public int quantity;
	
	public Consumption()
	{
		
	}
	
	public Consumption(long userId, int quantity)
	{
		this.userId=userId;
		this.quantity=quantity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
