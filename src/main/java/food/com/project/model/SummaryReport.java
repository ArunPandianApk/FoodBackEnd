package food.com.project.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SummaryReport 
{
	@Id
	String name;
	float averageConsumption;
	int numberOfDays,numberOfPersons;
	
	SummaryReport()
	{
		
	}
	
	SummaryReport(String name, float averageConsumption)
	{
		this.name = name;
		this.averageConsumption = averageConsumption;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public float getAverageConsumption() 
	{
		return averageConsumption;
	}

	public void setAverageConsumption(float averageConsumption) 
	{
		this.averageConsumption = averageConsumption;
	}

	public int getNumberOfDays() 
	{
		return numberOfDays;
	}

	public void setNumberOfDays(int numberOfDays) 
	{
		this.numberOfDays = numberOfDays;
	}

	public int getNumberOfPersons() 
	{
		return numberOfPersons;
	}

	public void setNumberOfPersons(int numberOfPersons) 
	{
		this.numberOfPersons = numberOfPersons;
	}
}
