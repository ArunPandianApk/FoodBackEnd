package food.com.project.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SuperUser 
{
	@Id
	long id;
	String name,password;
	String authenticationKey;
	@Column(nullable = false, columnDefinition = "timestamp default current_timestamp")
	Timestamp lastActiveTimestamp;
	
	public SuperUser()
	{
		
	}
	
	public SuperUser(long id, String name, String password)
	{
		this.id=id;
		this.name=name;
		this.password=password;
		lastActiveTimestamp = new Timestamp(System.currentTimeMillis());
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	public long getId() 
	{
		return this.id;
	}

	public String getAuthenticationKey() 
	{
		return authenticationKey;
	}

	public void setAuthenticationKey(String authenticationKey) 
	{
		this.authenticationKey = authenticationKey;
	}

	public Timestamp getLastActiveTimestamp() {
		return lastActiveTimestamp;
	}

	public void setLastActiveTimestamp(Timestamp lastActiveTimestamp) {
		this.lastActiveTimestamp = lastActiveTimestamp;
	}
}
