package food.com.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Users
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String name;
	public boolean isActive;
	
	public Users()
	{
		
	}
	
	public Users(long id,String name)
	{
		this.id=id;
		this.name=name;
	}
	
	public boolean isActive() 
	{
		return isActive;
	}

	public void setActive(boolean isActive) 
	{
		this.isActive = isActive;
	}

	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name=name;
	}
	
	public void setId(long id)
	{
		this.id=id;
	}

	public long getId() 
	{
		return this.id;
	}
}
