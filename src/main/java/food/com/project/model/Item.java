package food.com.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String name;
	public boolean isDelivered;
	
	public boolean isDelivered() 
	{
		return isDelivered;
	}

	public void setDelivered(boolean isDelivered) 
	{
		this.isDelivered = isDelivered;
	}

	public Item()
	{
		
	}
	
	public Item(long id,String name)
	{
		this.id=id;
		this.name=name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name=name;
	}
	
	public void setId(long id)
	{
		this.id=id;
	}

	public long getId() {
		return id;
	}
}
