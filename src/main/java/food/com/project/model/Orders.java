package food.com.project.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Orders
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public long itemId;
	public Date date;
	public int totalQuantity;
	public boolean isEnded;
	
	public Orders()
	{
		
	}
	
	public Orders(long itemId, int totalQuantity)
	{
		this.itemId=itemId;
		this.totalQuantity=totalQuantity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	public String toString()
	{
		return this.itemId+"";
	}

	public boolean isEnded() 
	{
		return isEnded;
	}

	public void setEnded(boolean isEnded) 
	{
		this.isEnded = isEnded;
	}
}
