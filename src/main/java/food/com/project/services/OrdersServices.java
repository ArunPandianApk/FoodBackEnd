package food.com.project.services;

import java.sql.Date;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import food.com.project.model.Orders;
import food.com.project.repository.OrdersRepository;

@Service
public class OrdersServices 
{
	@Autowired
	OrdersRepository ordersRepository;
	
	public Collection<Orders> getLog()
	{
		return ordersRepository.findAll();
	}
	
	public void addOrders(Orders orders)
	{
		orders.setDate(new Date(System.currentTimeMillis()));
		Orders yesterdaysOrder = ordersRepository.getYesderdaysOrder();
		if(yesterdaysOrder != null && !yesterdaysOrder.isEnded())
		{
			yesterdaysOrder.setEnded(true);
			ordersRepository.save(yesterdaysOrder);
		}
		ordersRepository.save(orders);
	}
	
	public Orders getStatus()
	{
		Orders orders=ordersRepository.getTodaysOrder();
		if(orders == null)
			return new Orders();
		return orders;
			
	}
	
	public void endSession()
	{
		ordersRepository.endSession();
	}
}
