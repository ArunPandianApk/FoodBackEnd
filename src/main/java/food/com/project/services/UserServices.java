package food.com.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import food.com.project.model.Users;
import food.com.project.repository.UserRepository;

@Service
public class UserServices 
{
	@Autowired
	UserRepository userRepository;
	
	public List<Users> getUsersOrderByName()
	{
		return userRepository.findAllByName();
	}
	
	public List<Users> getUsers()
	{
		return userRepository.findActive();
	}
	
	public List<String> getUserNames()
	{
		return userRepository.findActiveNames();
	}

	public void deactivateUser(long id)
	{
		userRepository.deactivate(id);
	}
	
	public void activateUser(long id)
	{
		userRepository.activate(id);
	}
	
	public void addUser(Users users)
	{
		userRepository.save(users);
	}
	
	public Users getUserByName(String name)
	{
		return userRepository.getUserByName(name);
	}
}
