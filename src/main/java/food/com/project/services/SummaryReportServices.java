package food.com.project.services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import food.com.project.model.SummaryReport;
import food.com.project.repository.SummaryReportRepository;

@Service
public class SummaryReportServices 
{
	@Autowired
	SummaryReportRepository summaryReportRepository;
	
	public Collection<SummaryReport> getSummary()
	{
		return summaryReportRepository.getSummary();
	}

	public Collection<SummaryReport> getOverallSummary()
	{
		return summaryReportRepository.getOverallSummary();
	}
}
