package food.com.project.services;

import food.com.project.model.SuperUser;
import food.com.project.repository.SuperUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Random;

@Service
public class SuperUserServices 
{
	@Autowired
	SuperUserRepository superUserRepository;

	public SuperUser getSuperUser(long id) {
		try
		{
			return superUserRepository.findById(id).get();
		}
		catch(Exception e)
		{
			return new SuperUser();
		}
	}
	
	public void addUser(SuperUser superUser)
	{
		superUserRepository.save(superUser);
	}
	
	public ResponseEntity<String> login(SuperUser user, HttpServletResponse response)
	{
		SuperUser superUser = superUserRepository.findById(user.getId()).get();
		if(user.getName().equals(superUser.getName()) && user.getPassword().equals(superUser.getPassword()))
            return new ResponseEntity<String>("true", changeAuthenticationKey(superUser, generateTokens(), response), HttpStatus.OK);
		return new ResponseEntity<String>("false", new HttpHeaders(), HttpStatus.OK);
	}
	
	private HttpHeaders changeAuthenticationKey(SuperUser superUser, String key, HttpServletResponse response)
	{
		HttpHeaders headers = new HttpHeaders();
		superUser.setAuthenticationKey(key);
		superUser.setLastActiveTimestamp(new Timestamp(System.currentTimeMillis()));
		superUserRepository.save(superUser);
		Cookie cookie = new Cookie(superUser.getName(), key);
		cookie.setPath("/");
		response.addCookie(cookie);
        headers.add("Cache-Control", "no-store, no-cache");
		return headers;
	}
	
	public ResponseEntity<String> checkAuthentication(long id, HttpServletRequest request, HttpServletResponse response)
	{
		SuperUser superUser = superUserRepository.findById(id).get();
		int timeout = id == 1 ? 500 : 5*60*60;
		if(superUserRepository.getTimeInterval(id) > timeout)
		{
			changeAuthenticationKey(superUser, generateTokens(), response);
			return new ResponseEntity<String>("false", new HttpHeaders(), HttpStatus.OK);
		}
		Cookie cookies[] = request.getCookies();
		for(Cookie cookie: cookies)
			if(cookie.getName().equals(superUser.getName()) && cookie.getValue().equals(superUser.getAuthenticationKey()))
				return new ResponseEntity<String>("true", changeAuthenticationKey(superUser, cookie.getValue(), response), HttpStatus.OK);
		return new ResponseEntity<String>("false", new HttpHeaders(), HttpStatus.OK);
	}
	
	private String generateTokens()
	{
		int x;
		String alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		Random random = new Random();
		StringBuilder builder=new StringBuilder();
		for(int i=0; i<20; i++)
		{
			x = random.nextInt()%alphabets.length();
			if(x > 0)
				builder.append(alphabets.charAt(x));
			else
				i--;
		}
		return builder.toString();
	}
	
	public void logout()
	{
		superUserRepository.logout();
	}

    public boolean changePassword(String username, String password, String newpassword)
    {
        SuperUser superUser = superUserRepository.getUserByName(username);
        if(superUser.getPassword().equals(password))
        {
            superUserRepository.save(new SuperUser(superUser.getId(), username, newpassword));
            return true;
        }
        return false;
    }
}