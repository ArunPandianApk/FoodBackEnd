package food.com.project.services;

import java.sql.Date;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import food.com.project.model.Consumption;
import food.com.project.repository.ConsumptionRepository;

@Service
public class ConsumptionServices 
{
	@Autowired
	ConsumptionRepository consumptionRepository;

	public ResponseEntity<String> checkConsumption(long id)
	{
		if(consumptionRepository.getConsumption(id) == null)
			return new ResponseEntity<String>("true", new HttpHeaders(), HttpStatus.OK);
		return new ResponseEntity<String>("false", new HttpHeaders(), HttpStatus.OK);
	}
	
	public void addConsumption(Consumption consumption)
	{
		consumption.setDate(new Date(System.currentTimeMillis()));
		consumptionRepository.save(consumption);
	}
}
