package food.com.project.services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import food.com.project.model.Item;
import food.com.project.repository.ItemRepository;

@Service
public class ItemServices 
{
	@Autowired
	ItemRepository itemRepository;
	
	public Collection<Item> getAllItems()
	{
		return itemRepository.findAllOrderByName();
	}
	
	public Collection<Item> getItems()
	{
		return itemRepository.findActive();
	}
	
	public void deactivateItem(long id)
	{
		itemRepository.stopDelivery(id);
	}
	
	public void activateItem(long id)
	{
		itemRepository.startDelivery(id);
	}
	
	public void addItem(Item item)
	{
		itemRepository.save(item);
	}
	
	public void deleteItem(long id)
	{
		itemRepository.deleteById(id);
	}
	
	public Item getIdByName(String name)
	{
		return itemRepository.getIdByName(name);
	}
	
	public Collection<String> getItemsName()
	{
		return itemRepository.getNames();
	}
}
