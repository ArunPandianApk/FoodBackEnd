package food.com.project.controller;

import java.util.Collection;

import food.com.project.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import food.com.project.model.Orders;
import food.com.project.services.OrdersServices;

@RestController
@CrossOrigin(origins = { Constants.LOCAL_ORIGIN, Constants.REMOTE_ORIGIN }, allowCredentials = "true")
public class OrdersController
{
	@Autowired
	OrdersServices ordersServices;
	
	@RequestMapping(value="/orders", method=RequestMethod.GET)
	ResponseEntity<Collection<Orders>> getLog()
	{
        return new ResponseEntity<Collection<Orders>>(ordersServices.getLog(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/orders/add", method=RequestMethod.POST)
	void addOrders(@RequestBody Orders orders)
	{
		ordersServices.addOrders(orders);
	}

	@RequestMapping(value="/orders/session", method=RequestMethod.GET)
	ResponseEntity<Orders> getStatus()
    {
		return new ResponseEntity<Orders>(ordersServices.getStatus(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/orders/end", method=RequestMethod.GET)
	void endSession()
	{
		ordersServices.endSession();
	}
}
