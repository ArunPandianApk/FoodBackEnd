package food.com.project.controller;

import java.util.Collection;

import food.com.project.model.Orders;
import food.com.project.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import food.com.project.model.Item;
import food.com.project.services.ItemServices;

@RestController
@CrossOrigin(origins = { Constants.LOCAL_ORIGIN, Constants.REMOTE_ORIGIN }, allowCredentials = "true")
public class ItemController 
{
	@Autowired
	ItemServices itemServices;

	@RequestMapping(value="/item/all", method=RequestMethod.GET)
	ResponseEntity<Collection<Item>> getAllItems()
	{
		return new ResponseEntity<Collection<Item>>(itemServices.getAllItems(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/item", method=RequestMethod.GET)
	ResponseEntity<Collection<Item>> getItems()
	{
		return new ResponseEntity<Collection<Item>>(itemServices.getItems(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/item/stopdeliver/{id}", method=RequestMethod.GET)
	void deactivateItem(@PathVariable("id") long id)
	{
		itemServices.deactivateItem(id);
	}
	
	@RequestMapping(value="/item/deliver/{id}", method=RequestMethod.GET)
	void activateItem(@PathVariable("id") long id)
	{
		itemServices.activateItem(id);
	}
	
	@RequestMapping(value="/item/add", method=RequestMethod.POST)
	void addItem(@RequestBody Item item)
	{
		itemServices.addItem(item);
	}
	
	@RequestMapping(value="/item/{id}", method=RequestMethod.DELETE)
	void deleteItem(@PathVariable("id") long id)
	{
		itemServices.deleteItem(id);
	}
	
	@RequestMapping(value="/item/{name}", method=RequestMethod.GET)
	ResponseEntity<Item> getIdByName(@PathVariable("name") String name)
	{
		return new ResponseEntity<Item>(itemServices.getIdByName(name), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/item/name", method=RequestMethod.GET)
	ResponseEntity<Collection<String>> getItemsName()
	{
        return new ResponseEntity<Collection<String>>(itemServices.getItemsName(), new HttpHeaders(), HttpStatus.OK);
	}
}