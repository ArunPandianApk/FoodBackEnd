package food.com.project.controller;

import java.util.Collection;

import food.com.project.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import food.com.project.model.Consumption;
import food.com.project.services.ConsumptionServices;

@RestController
@CrossOrigin(origins = { Constants.LOCAL_ORIGIN, Constants.REMOTE_ORIGIN }, allowCredentials = "true")
public class ConsumptionController 
{
	@Autowired
	ConsumptionServices consumptionServices;

	@RequestMapping(value="/consumption/check/{id}", method=RequestMethod.GET)
	ResponseEntity<String> checkConsumption(@PathVariable("id") long id)
	{
		return consumptionServices.checkConsumption(id);
	}
	
	@RequestMapping(value="/consumption/add", method=RequestMethod.POST)
	void addConsumption(@RequestBody Consumption consumption)
	{
		consumptionServices.addConsumption(consumption);
	}
}