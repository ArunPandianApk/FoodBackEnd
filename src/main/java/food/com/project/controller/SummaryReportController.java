package food.com.project.controller;

import java.util.Collection;

import food.com.project.model.Item;
import food.com.project.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import food.com.project.model.SummaryReport;
import food.com.project.services.SummaryReportServices;

@RestController
@CrossOrigin(origins = { Constants.LOCAL_ORIGIN, Constants.REMOTE_ORIGIN }, allowCredentials = "true")
public class SummaryReportController 
{
	@Autowired
	SummaryReportServices summaryReportServices;

	@RequestMapping(value="/report/summary", method=RequestMethod.GET)
	ResponseEntity<Collection<SummaryReport>> getSummary()
	{
		return new ResponseEntity<Collection<SummaryReport>>(summaryReportServices.getSummary(), new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value="/report/overallsummary", method=RequestMethod.GET)
	ResponseEntity<Collection<SummaryReport>> getOverallSummary()
	{
		return new ResponseEntity<Collection<SummaryReport>>(summaryReportServices.getOverallSummary(), new HttpHeaders(), HttpStatus.OK);
	}
}
