package food.com.project.controller;

import java.util.List;

import food.com.project.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import food.com.project.model.Users;
import food.com.project.services.UserServices;

@RestController
@CrossOrigin(origins = { Constants.LOCAL_ORIGIN, Constants.REMOTE_ORIGIN }, allowCredentials = "true")
public class UserController 
{
	@Autowired
	UserServices userServices;

	@RequestMapping(value="/users/all", method=RequestMethod.GET)
	public ResponseEntity<List<Users>> getActiveUsers()
	{
		return new ResponseEntity<List<Users>>(userServices.getUsersOrderByName(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/users", method=RequestMethod.GET)
	ResponseEntity<List<Users>> getUsers()
	{
		return new ResponseEntity<List<Users>>(userServices.getUsers(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/users/name", method=RequestMethod.GET)
	ResponseEntity<List<String>> getUsersName()
	{
        return new ResponseEntity<List<String>>(userServices.getUserNames(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/users/deactivate/{id}", method=RequestMethod.GET)
	void deactivateUser(@PathVariable("id") long id)
	{
		userServices.deactivateUser(id);
	}
	
	@RequestMapping(value="/users/activate/{id}", method=RequestMethod.GET)
	void activateUser(@PathVariable("id") long id)
	{
		userServices.activateUser(id);
	}
	
	@RequestMapping(value="/users/add", method=RequestMethod.POST)
	void addUser(@RequestBody Users users)
	{
		userServices.addUser(users);
	}
	
	@RequestMapping(value="/users/{name}", method=RequestMethod.GET)
	ResponseEntity<Users> getUserByName(@PathVariable("name") String name)
	{
		return new ResponseEntity<Users>(userServices.getUserByName(name), new HttpHeaders(), HttpStatus.OK);
	}
}