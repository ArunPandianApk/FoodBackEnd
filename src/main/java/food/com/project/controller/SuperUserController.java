package food.com.project.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import food.com.project.model.Item;
import food.com.project.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import food.com.project.model.SuperUser;
import food.com.project.services.SuperUserServices;

import java.util.Map;

@RestController
@CrossOrigin(origins = { Constants.LOCAL_ORIGIN, Constants.REMOTE_ORIGIN }, allowCredentials = "true")
public class SuperUserController 
{
	@Autowired
	SuperUserServices superUserServices;

	@RequestMapping(value="/superuser/{id}", method=RequestMethod.GET)
	ResponseEntity<SuperUser> getAdmin(@PathVariable("id") long id)
	{
		return new ResponseEntity<SuperUser>(superUserServices.getSuperUser(id), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/superuser/change", method=RequestMethod.POST)
	ResponseEntity<String> changePassword(@RequestBody Map<String, String> values)
    {
        System.out.println(values.get("username"));
        System.out.println(values.get("password"));
        System.out.println(values.get("newpassword"));
        boolean x = superUserServices.changePassword(values.get("username"),values.get("password"), values.get("newpassword"));
        System.out.println(x);
        if(x)
            return new ResponseEntity<String>("true", new HttpHeaders(), HttpStatus.OK);
        return new ResponseEntity<String>("false", new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/superuser/login", method=RequestMethod.POST)
	ResponseEntity<String> login(@RequestBody SuperUser user, HttpServletResponse response)
	{
		return superUserServices.login(user, response);
	}
	
	@RequestMapping(value="/superuser/check/{id}", method=RequestMethod.GET)
	ResponseEntity<String> checkAuthentication(@PathVariable("id") long id, HttpServletRequest request, HttpServletResponse response)
	{
		return superUserServices.checkAuthentication(id, request, response);
	}
	
	@RequestMapping(value="/superuser/logout", method=RequestMethod.GET)
	void logout()
	{
		superUserServices.logout();
	}
}