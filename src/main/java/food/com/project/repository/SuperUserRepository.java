package food.com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import food.com.project.model.SuperUser;

import javax.transaction.Transactional;

@Repository
public interface SuperUserRepository extends JpaRepository<SuperUser,Long>
{
	@Query(value="select extract(epoch from current_timestamp-last_active_timestamp) from super_user where id=?1", nativeQuery=true)
	int getTimeInterval(long id);

	@Transactional
	@Modifying
	@Query(value="update super_user set authentication_key='' where id=1", nativeQuery=true)
	void logout();

	@Query(value = "select * from super_user where name = ?1", nativeQuery = true)
    SuperUser getUserByName(String password);
}
