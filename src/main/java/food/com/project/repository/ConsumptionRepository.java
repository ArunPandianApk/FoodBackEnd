package food.com.project.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import food.com.project.model.Consumption;

@Repository
public interface ConsumptionRepository extends JpaRepository<Consumption,Long>
{
	@Query(value="select * from consumption where date=current_date and user_id=?1", nativeQuery=true)
	Consumption getConsumption(long id);
}