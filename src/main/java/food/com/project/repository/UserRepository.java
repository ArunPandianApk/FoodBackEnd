package food.com.project.repository;

import food.com.project.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserRepository extends JpaRepository<Users,Long>
{
	@Query(value="select * from users where is_active=true", nativeQuery=true)
	List<Users> findActive();

	@Transactional
	@Modifying
	@Query(value="update users set is_active=false where id=?1", nativeQuery=true)
	void deactivate(long id);

	@Transactional
	@Modifying
	@Query(value="update users set is_active=true where id=?1", nativeQuery=true)
	void activate(long id);
	
	@Query(value="select * from users order by name", nativeQuery=true)
	List<Users> findAllByName();

	@Query(value="select name from users where is_active=true order by name", nativeQuery=true)
	List<String> findActiveNames();

	@Query(value = "select * from users where name=?1", nativeQuery = true)
	Users getUserByName(String name);
}
