package food.com.project.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import food.com.project.model.Item;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ItemRepository extends JpaRepository<Item,Long>
{
	@Query(value="select * from item where name=?1", nativeQuery=true)
	Item getIdByName(String name);

	@Query(value="select * from item where is_delivered=true", nativeQuery=true)
	Collection<Item> findActive();

	@Transactional
	@Modifying
	@Query(value="update item set is_delivered=false where id=?1", nativeQuery=true)
	void stopDelivery(long id);

	@Transactional
	@Modifying
	@Query(value="update item set is_delivered=true where id=?1", nativeQuery=true)
	void startDelivery(long id);
	
	@Query(value="select * from item order by name", nativeQuery=true)
	Collection<Item> findAllOrderByName();
	
	@Query(value="select name from item where is_delivered=true order by name", nativeQuery=true)
	Collection<String> getNames();
}
