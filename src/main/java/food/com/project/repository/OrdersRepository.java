package food.com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import food.com.project.model.Orders;

import javax.transaction.Transactional;

public interface OrdersRepository extends JpaRepository<Orders,Long>
{
	@Query(value="select * from orders where date=current_date", nativeQuery=true)
	Orders getTodaysOrder();

	@Query(value="select * from orders where date=current_date-1", nativeQuery=true)
	Orders getYesderdaysOrder();

	@Transactional
	@Modifying
	@Query(value="update orders set is_ended=true where date=current_date", nativeQuery=true)
	void endSession();
}
