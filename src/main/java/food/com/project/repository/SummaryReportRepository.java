package food.com.project.repository;

import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import food.com.project.model.SummaryReport;

@Repository
public interface SummaryReportRepository extends JpaRepository<SummaryReport,Long> 
{
	@Query(value="select name, round(avg(quantity),1) as average_consumption, count(distinct orders.date) as number_of_days, count(consumption.id) as number_of_persons from item inner join orders on item_id = item.id inner join consumption on consumption.date = orders.date where consumption.date>current_date-4 group by name order by name;", nativeQuery=true)
	Collection<SummaryReport> getSummary();
	
	@Query(value="select name, round(avg(quantity),1) as average_consumption, count(distinct orders.date) as number_of_days, count(consumption.id) as number_of_persons from item inner join orders on item_id = item.id inner join consumption on consumption.date = orders.date group by name order by name", nativeQuery=true)
	Collection<SummaryReport> getOverallSummary();
}
