package food.com.project.utils;

public class Constants
{
    public static final String LOCAL_ORIGIN = "http://localhost:3000";
    public static final String REMOTE_ORIGIN = "https://ajira-food.herokuapp.com";
}
