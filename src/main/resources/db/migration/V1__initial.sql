create table users (id serial primary key, name varchar(255) not null, is_active boolean default true);

create table item (id serial primary key, name varchar(255) not null, is_delivered boolean default true);

create table orders (id serial primary key, item_id bigint not null references item(id), date date not null unique, total_quantity int not null, is_ended boolean not null default false);

create table consumption (id serial primary key, user_id bigint not null references users(id), date date not null references orders(date), quantity int not null);

create table super_user (id serial primary key, name varchar(255) not null, password varchar not null, authentication_key varchar(20), last_active_timestamp timestamp not null default current_timestamp);