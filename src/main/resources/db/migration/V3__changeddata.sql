drop table consumption;

drop table users;

drop table orders;

drop table item;

create table users (id serial primary key, name varchar(255) not null, is_active boolean default true);

create table item (id serial primary key, name varchar(255) not null, is_delivered boolean default true);

create table orders (id serial primary key, item_id bigint not null references item(id), date date not null unique, total_quantity int not null, is_ended boolean not null default false);

create table consumption (id serial primary key, user_id bigint not null references users(id), date date not null references orders(date), quantity int not null);

insert into users(name, is_active) values ('Arun Pandian', true),('Shimo', true),('Kanishka', true),('DD', true),('Saravanan', true),('Gowthamraj', true),('Tamil', true),('Raghavan', true),('Sat', true),('Prabhu', true);


insert into item(name, is_delivered) values ('Dosa', true),('Idiyappam', true),('Idly', true),('Poori', true);

insert into orders(item_id, date, total_quantity, is_ended) values (1, current_date-3, 100, true),(2, current_date-2, 100, true),(3, current_date-1, 100, true),(4, current_date, 100, true);

insert into consumption(user_id, date, quantity) values (1, current_date-3, 7),(2, current_date-3, 6),(3, current_date-3, 2),(4, current_date-3, 9),(5, current_date-3, 2),(6, current_date-3, 3),(7, current_date-3, 7),(8, current_date-3, 2),(9, current_date-3, 9),(10, current_date-3, 6),
							   (1, current_date-2, 5),(2, current_date-2, 8),(3, current_date-2, 4),(4, current_date-2, 5),(5, current_date-2, 3),(6, current_date-2, 6),(7, current_date-2, 2),(8, current_date-2, 2),(9, current_date-2, 5),(10, current_date-2, 5),
							   (1, current_date-1, 7),(2, current_date-1, 2),(3, current_date-1, 7),(4, current_date-1, 5),(5, current_date-1, 2),(6, current_date-1, 4),(7, current_date-1, 5),(8, current_date-1, 7),(9, current_date-1, 3),(10, current_date-1, 4),
							   (1, current_date, 4),(2, current_date, 5),(3, current_date, 3),(4, current_date, 2),(5, current_date, 7),(6, current_date, 3),(7, current_date, 7),(8, current_date, 5),(9, current_date, 2),(10, current_date, 7);