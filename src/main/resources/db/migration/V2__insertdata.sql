insert into users values (1,'Arun Pandian', true),(2,'Shimo', true),(3,'Kanishka', true),(4,'DD', true),(5,'Saravanan', true),(6,'Gowthamraj', true),(7,'Tamil', true),(8,'Raghavan', true),(9,'Sat', true),(10,'Prabhu', true);

insert into item values (1,'Dosa', true),(2,'Idiyappam', true),(3,'Idly', true),(4,'Poori', true);

insert into orders values (1, 1, current_date-3, 100, true),(2, 2, current_date-2, 100, true),(3, 3, current_date-1, 100, true),(4, 4, current_date, 100, true);

insert into consumption values (1, 1, current_date-3, 7),(2, 2, current_date-3, 6),(3, 3, current_date-3, 2),(4, 4, current_date-3, 9),(5, 5, current_date-3, 2),(6, 6, current_date-3, 3),(7, 7, current_date-3, 7),(8, 8, current_date-3, 2),(9, 9, current_date-3, 9),(10, 10, current_date-3, 6),
							   (11, 1, current_date-2, 5),(12, 2, current_date-2, 8),(13, 3, current_date-2, 4),(14, 4, current_date-2, 5),(15, 5, current_date-2, 3),(16, 6, current_date-2, 6),(17, 7, current_date-2, 2),(18, 8, current_date-2, 2),(19, 9, current_date-2, 5),(20, 10, current_date-2, 5),
							   (21, 1, current_date-1, 7),(22, 2, current_date-1, 2),(23, 3, current_date-1, 7),(24, 4, current_date-1, 5),(25, 5, current_date-1, 2),(26, 6, current_date-1, 4),(27, 7, current_date-1, 5),(28, 8, current_date-1, 7),(29, 9, current_date-1, 3),(30, 10, current_date-1, 4),
							   (31, 1, current_date, 4),(32, 2, current_date, 5),(33, 3, current_date, 3),(34, 4, current_date, 2),(35, 5, current_date, 7),(36, 6, current_date, 3),(37, 7, current_date, 7),(38, 8, current_date, 5),(39, 9, current_date, 2),(40, 10, current_date, 7);

insert into super_user values (1,'Admin','12345','',current_timestamp),(2,'Vendor','98765','',current_timestamp);